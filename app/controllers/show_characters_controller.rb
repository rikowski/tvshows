class ShowCharactersController < ApplicationController
  before_action :set_show_character, only: [:show, :edit, :update, :destroy]

  # GET /show_characters
  # GET /show_characters.json
  def index
    @show_characters = ShowCharacter.all
  end

  # GET /show_characters/1
  # GET /show_characters/1.json
  def show
  end

  # GET /show_characters/new
  def new
    @show_character = ShowCharacter.new
  end

  # GET /show_characters/1/edit
  def edit
  end

  # POST /show_characters
  # POST /show_characters.json
  def create
    @show_character = ShowCharacter.new(show_character_params)

    respond_to do |format|
      if @show_character.save
        format.html { redirect_to @show_character, notice: 'Show character was successfully created.' }
        format.json { render :show, status: :created, location: @show_character }
      else
        format.html { render :new }
        format.json { render json: @show_character.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /show_characters/1
  # PATCH/PUT /show_characters/1.json
  def update
    respond_to do |format|
      if @show_character.update(show_character_params)
        format.html { redirect_to @show_character, notice: 'Show character was successfully updated.' }
        format.json { render :show, status: :ok, location: @show_character }
      else
        format.html { render :edit }
        format.json { render json: @show_character.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /show_characters/1
  # DELETE /show_characters/1.json
  def destroy
    @show_character.destroy
    respond_to do |format|
      format.html { redirect_to show_characters_url, notice: 'Show character was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_show_character
      @show_character = ShowCharacter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def show_character_params
      params[:show_character].permit(:show_id, :character_id)
    end
end
