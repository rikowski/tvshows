json.array!(@show_characters) do |show_character|
  json.extract! show_character, :id
  json.url show_character_url(show_character, format: :json)
end
