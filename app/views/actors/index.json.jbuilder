json.array!(@actors) do |actor|
  json.extract! actor, :id, :name, :gender, :birth_year
  json.url actor_url(actor, format: :json)
end
