json.array!(@shows) do |show|
  json.extract! show, :id, :name, :genre, :premiere_year, :season_count, :episode_count, :running
  json.url show_url(show, format: :json)
end
