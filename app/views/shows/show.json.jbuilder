json.extract! @show, :id, :name, :genre, :premiere_year, :season_count, :episode_count, :running, :created_at, :updated_at
