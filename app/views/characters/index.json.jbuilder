json.array!(@characters) do |character|
  json.extract! character, :id, :name, :gender, :actor_id
  json.url character_url(character, format: :json)
end
