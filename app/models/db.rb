class DB < ActiveRecord::Base

  extend Hirb::Console

  self.abstract_class = true

  def self.sql(query)
    result = ActiveRecord::Base.connection.execute(query)
    result = result.map do |row|
      row.delete_if { |(k,v)| k.is_a?(Numeric) || %w(created_at updated_at).include?(k) }
    end

    table result
  end

end