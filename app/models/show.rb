class Show < ActiveRecord::Base

  has_many :show_characters
  has_many :characters, through: :show_characters

  def actors
    Actor.joins(:characters, :shows).where(shows: { id: id }).distinct
  end

end
