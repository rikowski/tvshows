class Character < ActiveRecord::Base

  belongs_to :actor

  has_many :show_characters
  has_many :shows, through: :show_characters


end
