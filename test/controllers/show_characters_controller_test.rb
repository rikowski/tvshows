require 'test_helper'

class ShowCharactersControllerTest < ActionController::TestCase
  setup do
    @show_character = show_characters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:show_characters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create show_character" do
    assert_difference('ShowCharacter.count') do
      post :create, show_character: {  }
    end

    assert_redirected_to show_character_path(assigns(:show_character))
  end

  test "should show show_character" do
    get :show, id: @show_character
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @show_character
    assert_response :success
  end

  test "should update show_character" do
    patch :update, id: @show_character, show_character: {  }
    assert_redirected_to show_character_path(assigns(:show_character))
  end

  test "should destroy show_character" do
    assert_difference('ShowCharacter.count', -1) do
      delete :destroy, id: @show_character
    end

    assert_redirected_to show_characters_path
  end
end
