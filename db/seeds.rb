Character.destroy_all
Show.destroy_all
ShowCharacter.destroy_all
Actor.destroy_all

Show.create!(
    [
        { name: 'The Simpsons', genre: 'Cartoon', premiere_year: 1989, season_count: 26, episode_count: 564, running: true },
        { name: 'Family Guy', genre: 'Cartoon', premiere_year: 1999, season_count: 13, episode_count: 240, running: true },
        { name: 'The Cleveland Show', genre: 'Cartoon', premiere_year: 2009, season_count: 4, episode_count: 88, running: false },
        { name: 'American Dad!', genre: 'Cartoon', premiere_year: 2005, season_count: 11, episode_count: 181, running: true },
        { name: 'Stargate SG-1', genre: 'SciFi', premiere_year: 1997, season_count: 10, episode_count: 214, running: false },
        { name: 'Stargate Atlantis', genre: 'SciFi', premiere_year: 2004, season_count: 5, episode_count: 100, running: false },
        { name: 'Star Trek: The Next Generation', genre: 'SciFi', premiere_year: 1987, season_count: 7, episode_count: 178, running: false },
        { name: 'Star Trek: Deep Space Nine', genre: 'SciFi', premiere_year: 1993, season_count: 7, episode_count: 176, running: false },
        { name: 'Star Trek: Voyager', genre: 'SciFi', premiere_year: 1995, season_count: 7, episode_count: 172, running: false },
        { name: 'Star Trek: Enterprise', genre: 'SciFi', premiere_year: 2001, season_count: 4, episode_count: 98, running: false },
        { name: 'Andromeda', genre: 'SciFi', premiere_year: 2000, season_count: 5, episode_count: 110, running: false },
        { name: 'Breaking Bad', genre: 'Drama', premiere_year: 2008, season_count: 5, episode_count: 62, running: false },
    ])

Actor.create!(
    [
        { name: 'Mike Henry', gender: 'm', birth_year: 1964 },
        { name: 'Sanaa Lathan', gender: 'f', birth_year: 1971 },
        { name: 'Reagan Gomez-Preston', gender: 'f', birth_year: 1980 },
        { name: 'Kevin Michael Richardson', gender: 'm', birth_year: 1964 },
        { name: 'Seth MacFarlane', gender: 'm', birth_year: 1973 },
        { name: 'Mila Kunis', gender: 'f', birth_year: 1983 },
        { name: 'Alex Borstein', gender: 'f', birth_year: 1973 },
        { name: 'Seth Green', gender: 'm', birth_year: 1974 },
        { name: 'Dan Castellaneta', gender: 'm', birth_year: 1957 },
        { name: 'Julie Kavner', gender: 'f', birth_year: 1950 },
        { name: 'Nancy Cartwright', gender: 'f', birth_year: 1957 },
        { name: 'Yeardley Smith', gender: 'f', birth_year: 1964 },
        { name: 'Patrick Steward', gender: 'm', birth_year: 1940 },
        { name: 'Robert Picardo', gender: 'm', birth_year: 1953 },
        { name: 'John de Lancie', gender: 'm', birth_year: 1948 },
        { name: 'Michael Dorn', gender: 'm', birth_year: 1952 },
        { name: 'Colm Meaney', gender: 'm', birth_year: 1953 },
        { name: 'Jonathan Frakes', gender: 'm', birth_year: 1952 },
        { name: 'Brent Spiner', gender: 'm', birth_year: 1949 },
        { name: 'Marina Sirtis', gender: 'f', birth_year: 1955 },
        { name: 'Gates McFadden', gender: 'f', birth_year: 1949 },
        { name: 'Avery Brooks', gender: 'm', birth_year: 1948 },
        { name: 'Nana Visitor', gender: 'f', birth_year: 1957 },
        { name: 'Alexander Siddig', gender: 'm', birth_year: 1965 },
        { name: 'Terry Farrel', gender: 'f', birth_year: 1963 },
        { name: 'Kate Mulgrew', gender: 'f', birth_year: 1959 },
        { name: 'Robert Beltran', gender: 'm', birth_year: 1953 },
        { name: 'Tim Russ', gender: 'm', birth_year: 1956 },
        { name: 'Robert Duncan McNeill', gender: 'm', birth_year: 1964 },
        { name: 'Roxann Dawson', gender: 'f', birth_year: 1958 },
        { name: 'Jeri Ryan', gender: 'f', birth_year: 1968 },
        { name: 'Wendy Schaal', gender: 'f', birth_year: 1954 },
        { name: 'Scott Grimes', gender: 'm', birth_year: 1971 },
        { name: 'Rachael MacFarlane', gender: 'f', birth_year: 1976 },
        { name: 'Dee Bradley Baker', gender: 'm', birth_year: 1962 },
        { name: 'Richard Dean Anderson', gender: 'm', birth_year: 1950 },
        { name: 'Michael Shanks', gender: 'm', birth_year: 1970 },
        { name: 'Amanda Tapping', gender: 'f', birth_year: 1965 },
        { name: 'Christopher Judge', gender: 'm', birth_year: 1964 },
        { name: 'Don Sinclair Davis', gender: 'm', birth_year: 1942, death_year: 2008 },
        { name: 'Teryl Rothery', gender: 'f', birth_year: 1962 },
        { name: 'Corin Nemec', gender: 'm', birth_year: 1971 },
        { name: 'Claudia Black', gender: 'f', birth_year: 1972 },
        { name: 'Ben Browder', gender: 'm', birth_year: 1962 },
        { name: 'Beau Bridges', gender: 'm', birth_year: 1941 },
        { name: 'Dwight Schultz', gender: 'm', birth_year: 1947 },
        { name: 'Kevin Sorbo', gender: 'm', birth_year: 1958 },
        { name: 'Lisa Ryder', gender: 'f', birth_year: 1970 },
        { name: 'Keith Hamilton Cobb', gender: 'm', birth_year: 1962 },
        { name: 'Gordon Michael Woolvett', gender: 'm', birth_year: 1970 },
        { name: 'Laura Bertram', gender: 'f', birth_year: 1978 },
        { name: 'Brent Stait', gender: 'm', birth_year: 1959 },
        { name: 'Lexa Doig', gender: 'f', birth_year: 1973 },
        { name: 'Brandy Ledford', gender: 'm', birth_year: 1969 },

        { name: 'Bryan Cranston', gender: 'm', birth_year: 1956 },
        { name: 'Anna Gunn', gender: 'f', birth_year: 1968 },
        { name: 'Aaron Paul', gender: 'm', birth_year: 1979 },
        { name: 'Dean Norris', gender: '', birth_year: 1963 },
        { name: 'Betsy Brandt', gender: 'f', birth_year: 1973 },
        { name: 'Roy Frank "RJ" Mitte III', gender: 'm', birth_year: 1992 },
        { name: 'Bob Odenkirk', gender: 'm', birth_year: 1962 },
        { name: 'Giancarlo Esposito', gender: 'm', birth_year: 1958 },
        { name: 'Jonathan Banks', gender: 'm', birth_year: 1947 },
        { name: 'Laura Fraser', gender: 'f', birth_year: 1976 },
        { name: 'Jesse Plemons', gender: 'm', birth_year: 1988 },

    # { name: '', gender: 'm', birth_year: 19 },

    ]

)


Character.create!(
    [
        { name: 'Homer Simpson', gender: 'm', shows: [Show.find_by!(name: 'The Simpsons')], actor: Actor.find_by!(name: 'Dan Castellaneta') },
        { name: 'Marge Simpson', gender: 'f', shows: [Show.find_by!(name: 'The Simpsons')], actor: Actor.find_by!(name: 'Julie Kavner') },
        { name: 'Lisa Simpson', gender: 'f', shows: [Show.find_by!(name: 'The Simpsons')], actor: Actor.find_by!(name: 'Yeardley Smith') },
        { name: 'Bart Simpson', gender: 'm', shows: [Show.find_by!(name: 'The Simpsons')], actor: Actor.find_by!(name: 'Nancy Cartwright') },
    ]
)

Character.create!(
    [
        { name: 'Peter Griffin', gender: 'm', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Seth MacFarlane') },
        { name: 'Megan Griffin', gender: 'f', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Mila Kunis') },
        { name: 'Chris Griffin', gender: 'm', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Seth Green') },
        { name: 'Lois Griffin', gender: 'f', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Alex Borstein') },
        { name: 'Tricia Takanawa', gender: 'f', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Alex Borstein') },
        { name: 'Brian Griffin', gender: 'm', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Seth MacFarlane') },
        { name: 'Stewie Griffin', gender: 'm', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Seth MacFarlane') },
        { name: 'Glen Quagmire', gender: 'm', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Seth MacFarlane') },
        { name: 'Consuela', gender: 'f', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Mike Henry') },
        { name: 'Cleveland Brown', gender: 'm', shows: [Show.find_by!(name: 'Family Guy'), Show.find_by!(name: 'The Cleveland Show')], actor: Actor.find_by!(name: 'Mike Henry') },
        { name: 'Bruce', gender: 'm', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Mike Henry') },
        { name: 'Herbert', gender: 'm', shows: [Show.find_by!(name: 'Family Guy')], actor: Actor.find_by!(name: 'Mike Henry') },
    ]
)


Character.create!(
    [
        { name: 'Roberta Tubbs', gender: 'f', shows: [Show.find_by!(name: 'The Cleveland Show')], actor: Actor.find_by!(name: 'Reagan Gomez-Preston') },
        { name: 'Donna Tubbs', gender: 'f', shows: [Show.find_by!(name: 'The Cleveland Show')], actor: Actor.find_by!(name: 'Sanaa Lathan') },
        { name: 'Cleveland Brown, Jr.', gender: 'm', shows: [Show.find_by!(name: 'The Cleveland Show')], actor: Actor.find_by!(name: 'Kevin Michael Richardson') },
        { name: 'Tim the Bear', gender: 'm', shows: [Show.find_by!(name: 'The Cleveland Show')], actor: Actor.find_by!(name: 'Seth MacFarlane') },
        { name: 'Rallo Tubbs', gender: 'm', shows: [Show.find_by!(name: 'The Cleveland Show')], actor: Actor.find_by!(name: 'Mike Henry') },
    ]
)

Character.create!(
    [
        { name: 'Stan Smith', gender: 'm', shows: [Show.find_by!(name: 'American Dad!')], actor: Actor.find_by!(name: 'Seth MacFarlane') },
        { name: 'Roger', gender: 'm', shows: [Show.find_by!(name: 'American Dad!')], actor: Actor.find_by!(name: 'Seth MacFarlane') },
        { name: 'Francine Smith', gender: 'f', shows: [Show.find_by!(name: 'American Dad!')], actor: Actor.find_by!(name: 'Wendy Schaal') },
        { name: 'Steve Smith', gender: 'm', shows: [Show.find_by!(name: 'American Dad!')], actor: Actor.find_by!(name: 'Scott Grimes') },
        { name: 'Hayley Smith', gender: 'f', shows: [Show.find_by!(name: 'American Dad!')], actor: Actor.find_by!(name: 'Rachael MacFarlane') },
        { name: 'Klaus Heissler', gender: 'm', shows: [Show.find_by!(name: 'American Dad!')], actor: Actor.find_by!(name: 'Dee Bradley Baker') },
        { name: 'Avery Bullock', gender: 'm', shows: [Show.find_by!(name: 'American Dad!')], actor: Actor.find_by!(name: 'Patrick Steward') },
    ]
)

Character.create!(
    [
        { name: 'Richard Woolsey', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1'), Show.find_by!(name: 'Stargate Atlantis')], actor: Actor.find_by!(name: 'Robert Picardo') },
        { name: 'Jack O\'Neill', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Richard Dean Anderson') },
        { name: 'Daniel Jackson', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Michael Shanks') },
        { name: 'Samantha Carter', gender: 'f', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Amanda Tapping') },
        { name: 'Teal\'c', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1'), Show.find_by!(name: 'Stargate Atlantis'), Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Christopher Judge') },
        { name: 'George Hammond', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Don Sinclair Davis') },
        { name: 'Janet Fraiser', gender: 'f', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Teryl Rothery') },
        { name: 'Jonas Quinn', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Corin Nemec') },
        { name: 'Vala Mal Doran', gender: 'f', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Claudia Black') },
        { name: 'Cameron Mitchell', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Ben Browder') },
        { name: 'Hank Landry', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'Beau Bridges') },
        { name: 'Frank Simmons', gender: 'm', shows: [Show.find_by!(name: 'Stargate SG-1')], actor: Actor.find_by!(name: 'John de Lancie') },
    ]
)

Character.create!(
    [
        { name: 'Jean-Luc Picard', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: The Next Generation')], actor: Actor.find_by!(name: 'Patrick Steward') },
        { name: 'William Riker', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: The Next Generation')], actor: Actor.find_by!(name: 'Jonathan Frakes') },
        { name: 'Worf', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: The Next Generation'), Show.find_by!(name: 'Star Trek: Deep Space Nine')], actor: Actor.find_by!(name: 'Michael Dorn') },
        { name: 'Data', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: The Next Generation')], actor: Actor.find_by!(name: 'Brent Spiner') },
        { name: 'Miles O\'Brien', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: The Next Generation'), Show.find_by!(name: 'Star Trek: Deep Space Nine')], actor: Actor.find_by!(name: 'Colm Meaney') },
        { name: 'Deanna Troi', gender: 'f', shows: [Show.find_by!(name: 'Star Trek: The Next Generation')], actor: Actor.find_by!(name: 'Marina Sirtis') },
        { name: 'Beverly Crusher', gender: 'f', shows: [Show.find_by!(name: 'Star Trek: The Next Generation')], actor: Actor.find_by!(name: 'Gates McFadden') },
        { name: 'Q', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: The Next Generation'), Show.find_by!(name: 'Star Trek: Deep Space Nine'), Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'John de Lancie') },
        { name: 'Reginald Barcley', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: The Next Generation'), Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'Dwight Schultz') },

    ]
)

Character.create!(
    [

        { name: 'Benjamin Sisko', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: Deep Space Nine')], actor: Actor.find_by!(name: 'Avery Brooks') },
        { name: 'Kira Nerys', gender: 'f', shows: [Show.find_by!(name: 'Star Trek: Deep Space Nine')], actor: Actor.find_by!(name: 'Nana Visitor') },
        { name: 'Julian Bashir', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: Deep Space Nine')], actor: Actor.find_by!(name: 'Alexander Siddig') },
        { name: 'Jadzia Dax', gender: 'f', shows: [Show.find_by!(name: 'Star Trek: Deep Space Nine')], actor: Actor.find_by!(name: 'Terry Farrel') },


        { name: 'Kathryn Janeway', gender: 'f', shows: [Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'Kate Mulgrew') },
        { name: 'Chakotay', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'Robert Beltran') },
        { name: 'Tuvok', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'Tim Russ') },
        { name: 'Tom Paris', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'Robert Duncan McNeill') },
        { name: 'B\'Ellana Torres', gender: 'f', shows: [Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'Roxann Dawson') },
        { name: 'The Doctor', gender: 'm', shows: [Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'Robert Picardo') },
        { name: 'Seven of Nine', gender: 'f', shows: [Show.find_by!(name: 'Star Trek: Voyager')], actor: Actor.find_by!(name: 'Jeri Ryan') },


        { name: 'Sid Barry', gender: 'm', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'John de Lancie') },
        { name: 'Dylan Hunt', gender: 'm', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Kevin Sorbo') }, # Two and a half men, too
        { name: 'Beka Valentine', gender: 'f', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Lisa Ryder') },
        { name: 'Tyr Anasazi', gender: 'm', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Keith Hamilton Cobb') },
        { name: 'Seamus Harper', gender: 'm', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Gordon Michael Woolvett') },
        { name: 'Trance Gemini', gender: 'f', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Laura Bertram') },
        { name: 'Rev Bem', gender: 'm', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Brent Stait') },
        { name: 'Rommie', gender: 'f', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Lexa Doig') }, # Stargate SG-1, too
        { name: 'Doyle', gender: 'f', shows: [Show.find_by!(name: 'Andromeda')], actor: Actor.find_by!(name: 'Brandy Ledford') }, # Stargate SG-1, Stargate Atlantis, Modern Family, Married with children, too


        { name: 'Donald Margolis', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'John de Lancie') },
    # { name: '', gender: '', actor: Actor.find_by!(name: '') },

    ]
)


Character.create!(
    [
        { name: 'Walter White', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Bryan Cranston') },
        { name: 'Skyler White', gender: 'f', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Anna Gunn') },
        { name: 'Jesse Pinkman', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Aaron Paul') },
        { name: 'Hank Schrader', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Dean Norris') },
        { name: 'Marie Schrader', gender: 'f', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Betsy Brandt') },
        { name: 'Walter White, Jr', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Roy Frank "RJ" Mitte III') },
        { name: 'Saul Goodman', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Bob Odenkirk') },
        { name: 'Gustavo "Gus" Fring', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Giancarlo Esposito') },
        { name: 'Michael "Mike" Ehrmantraut ', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Jonathan Banks') },
        { name: 'Lydia Rodarte-Quayle', gender: 'f', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Laura Fraser') },
        { name: 'Todd Alquist', gender: 'm', shows: [Show.find_by!(name: 'Breaking Bad')], actor: Actor.find_by!(name: 'Jesse Plemons') },

    ]
)

