class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.string :name
      t.string :genre
      t.integer :premiere_year
      t.integer :season_count
      t.integer :episode_count
      t.boolean :running

      t.timestamps
    end
  end
end
