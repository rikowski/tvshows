class AddPictures < ActiveRecord::Migration
  def change

    add_column :shows, :picture, :string
    add_column :actors, :picture, :string
    add_column :characters, :picture, :string

  end
end
