class CreateActors < ActiveRecord::Migration
  def change
    create_table :actors do |t|
      t.string :name
      t.string :gender
      t.integer :birth_year
      t.integer :death_year

      t.timestamps
    end
  end
end
