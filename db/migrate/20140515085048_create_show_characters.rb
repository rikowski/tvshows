class CreateShowCharacters < ActiveRecord::Migration
  def change
    create_table :show_characters do |t|
      t.references :show,       index: true
      t.references :character,  index: true

      t.timestamps
    end
  end
end
